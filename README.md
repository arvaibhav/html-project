#   box-model
it refer to in how html element are modeled in html and how dimension of thoses html element are derived from css property
CSS Box-Model Property
*   content :The content of the box, where text and images appear
*   Margin Property:  
    *   area outside the border. 
    *   The margin is transparent
*   Border Property 
*   Padding Property :
    *   Clears an area around the content. 
    *   The padding is transparent

#   vh, vw
*   size: 1vh means 1% of browser width
#   rem vs em
*   em is relative to parent size
*   rem is relative to root
*   REMs are a way of setting font-sizes based on the font-size of the root

#   Positioning
### 1.relative
*   it position the element relative to parent 
*   used when need to position relative to parent element 
### 2.Absolute
*   it position relative to parent which having position relative
*   if not found any it work relative to root file
### 3.Fixed
*   it absoulte with diference that it will be fixed in that position even on scrolling
### 4.static 
*   default normal position as per browser
### 5.float
*   it moves the element on its container 
*   allows the text , inline elemnts to wrap around it 
#   Responsive Design
*   it is idea that webpage should look resposive in different layout like in mobile ,tablet , desktop
*   media query technique is used to resolve this issue
    *   @media(min-width:992px)and (max-width:1199px)  example or tablet 
#   SELECTOR
*   CSS Selectors - When do you apply the following types of selectors?
*   type selector: [type=value]{}
*   class selector: .classname{}
*   id selector# idname{}
*   child selectors: parent > child{}
*   descendent selector:parent child{}
*   multiple select: class, other, other{}